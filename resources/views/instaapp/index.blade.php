@extends('layouts.app')

@section('title', 'InstaMed')

@push('css')
   <style>
      .delete-comment a {
         color: black;
      }
      .delete-comment a:hover {
         text-decoration: underline;
      }
      .delete-comment a:active {
         text-decoration: underline;
      }
   </style>
@endpush
@section('content')
   <div class="container" style="padding-top: 0;padding-left: 0;padding-right: 0;padding-bottom: 0;">
      @if (!$posts->isEmpty())
         @foreach ($posts as $post)
            <div class="row justify-content-center">
               <div class="col-md-12" style="padding-right: 0;">
                  <div class="card">
                     <div class="card-body p-0">
                        <div class="user-content">
                           <img class="img-circle" src="{{ getImage($post->users->avatar) }}" width="30px" height="30px" alt="User Avatar">
                           <a href="{{ route('user.profile', $post->users->username) }}"><span class="ml-2">{{ $post->users->name }}</span></a>
                        </div>
                        <a href="{{ route('user.post.detail', ['username' => $post->users->username, 'id' => $post->id]) }}">
                           <div class="image-content">
                              <img src="{{ getImage($post->image) }}" width="100%" alt="">
                           </div>
                        </a>
                        <div class="like-content">
                           <small class="text-muted" style="margin: 8px 4px 0px 0px">
                              <button data-id="{{ $post->id }}" class="like btn border-0 btn-sm mt-1 {{ \Auth::user()->hasLiked($post) ? 'like-post-'.$post->id.'' : '' }}">
                                 @if(\Auth::user()->hasLiked($post))
                                    <span style="font-size: 25px;color:red;" class="fas fa-heart data-{{ $post->id }}"></span>
                                 @else
                                    <span style="font-size: 25px;color:black;" class="fas fa-heart data-{{ $post->id }}"></span>
                                 @endif
                              </button>
                              <span class="like-count-{{ $post->id }}" data-id="{{ $post->id }}">{{ $post->likers()->count() }}</span> suka
                           </small>
                        </div>
                        <div class="content-body">
                           <b><a href="{{ route('user.profile', $post->users->username) }}"><span>{{ $post->users->username }}</span></a></b>
                           <span style="text-align: justify;">{{ $post->content }}</span>
                        </div>
                        <div class="card-footer card-comments pl-3 pr-3 pt-2 pb-1 {{ $post->comments->count() > 3 ? ' komentar-scroll' : ' komentar-normal' }}" style="background-color: transparent;">
                           @if (!$post->comments->isEmpty())
                              @foreach ($post->comments as $comment)
                                 <div class="card-comment" style="border-bottom-color: transparent;padding: 1px;">
                                    <img class="img-circle img-sm" src="{{ getImage($comment->users->avatar) }}" alt="User Image">
                                    <div class="comment-text">
                                       <span class="username">
                                          {{ $comment->users->username }}
                                          <span class="text-muted float-right">{{ date('d/m/Y', strtotime($comment->created_at)) }}
                                             @if ($comment->user_id == \Auth::user()->id)
                                                <div class="delete-comment text-right">
                                                   <a href="{{ route('delete-comment', $comment->id) }}">Hapus</a>
                                                </div>
                                             @endif
                                          </span>
                                       </span>
                                       <p>
                                          {{ $comment->comment }}
                                       </p>
                                    </div>
                                 </div>
                              @endforeach
                           @endif
                        </div>
                        <div class="card-footer pl-1 pr-1 pb-1 pt-0" style="background-color: transparent;">
                           <form action="{{ route('add-comment') }}" method="post">
                              @csrf
                              <div class="row" style="margin:10px;">
                                 <div class="column text-center" style="width: 91%;background-color: transparent;border:1px solid grey;border-radius: 10px;">
                                    <input type="text" style="border-radius: 20px;" placeholder="Tambahkan komentar..." class="input-comment" name="comment{{ $post['id'] }}">
                                    <input type="hidden" name="post_id" value="{{ $post['id'] }}">
                                    <input type="hidden" name="status" value="dashboard">
                                 </div>
                                 <div class="column text-right" style="width: 9%;background-color: transparent;">
                                    <a class="btn-kirim-komentar" onclick="$(this).closest('form').submit()" href="#"><span class="fas fa-paper-plane" style="font-size: 25px;"></span></a>
                                 </div>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            @endforeach
      @endif
   </div>
@endsection
@push('js')
   <script>
      $(document).ready(function() {
         $.ajaxSetup({
            headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
         });

         $('.like').click(function() {
            var id =$(this).data("id");
            var post_id = $(this).data("id");
            var like_count = $('.like-count-'+id).text();
            var cObj = $(this);
            var baseurl = window.location.origin+window.location.pathname;
            console.log(baseurl)
            $.ajax({
               type:'POST',
               url:'{{ url('add-like') }}',
               data:{id:post_id},
               success:function(data) {
                  if($(cObj).hasClass("like-post-"+id)){
                     $('.like-count-'+id).text(parseInt(like_count) - 1);
                     $(cObj).removeClass("like-post-"+id);
                     $(".data-"+id).css("color", "black");

                  } else {
                     $('.like-count-'+id).text(parseInt(like_count) + 1);
                     $(cObj).addClass("like-post-"+id);
                     $(".data-"+id).css("color", "red");
                  }
               }
            });
         });
      });
   </script>
@endpush