@extends('layouts.app')

@section('title', 'InstaMed | Profil')

@push('css')
   <style>
      .posts img {
         width: 100%;
         height: 200px;
      }

      .btn-edit-profil {
         border: 1px solid #c9c9c9;
         margin-left: 7px;
         font-size: 11px;
      }
   </style>
@endpush

@section('content')
<div class="container">
   <!--Foto profil & Nama-->
   <div class="row justify-content-center">
      <div class="col-md-12 m-1" style="padding-top: 20px;">
         <div class="row">
            <div class="col-md-12 col-xs-12 col-lg-12 col-sm-12">
               <div class="row">
                 <div class="column text-center" style="width: 25%;">
                    <img src="{{ getImage($user['avatar']) }}" class="img-circle elevation-2" style="width: 80px; height: 80px"  alt="">    
                 </div>
                 <div class="column text-center" style="width: 25%;margin-top: 15px;">{{ count($user->posts) }} <br> Posts</div>
                 <div class="column text-center" style="width: 25%;margin-top: 15px;">111 <br> Follower</div>
                 <div class="column text-center" style="width: 25%;margin-top: 15px;">111 <br> Following</div>
               </div>
               <div class="col-md-12 col-xs-12 col-lg-12 col-sm-12" style="margin-top: 10px;">
                  <h4 class="d-inline">{{ $user['name'] }}</h4>
               </div>
               <div class="col-md-12 col-xs-12 col-lg-12 col-sm-12" style="margin-top: 10px;text-align: justify;">
                  <p class="d-inline">{{ $user['bio'] }}</p>
               </div>
               <!-- @if (Request::get('profile') == 'profile')
                  <a class="btn btn-sm btn-edit-profil" href="{{ route('pengaturan') }}">Edit Profil</a>
                  <a class="btn btn-sm btn-edit-profil" href="{{ route('add-post-page') }}">Tambah Postingan</a>
               @endif  -->
            </div>
         </div>
      </div>
   </div>

   <div class="row justify-content-center">
      <div class="col-md-12 text-center">
      <a class="btn btn-sm btn-edit-profil" href="{{ route('add-post-page') }}"><span class="fas fa-plus" aria-hidden="true" style="font-size: 25px;"></span></a>
      </div>
      <div class="col-md-12 m-1">
         <div class="row">
            @if (!$posts->isEmpty())
               @foreach ($posts->chunk(3) as $chunk)
                  <div class="row">
                  @foreach($chunk as $post)
                     <div class="col-md-4" style="width: 33%;">
                        <a href="{{ route('user.post.detail', ['username' => $post->users->username, 'id' => $post->id]) }}">
                           <div class="card">
                              <div class="posts">
                                 <img src="{{ getImage($post->image) }}" class="img-thumbnail" alt="">
                              </div>
                           </div>
                        </a>
                     </div>
                  @endforeach
                  </div>
               @endforeach
            
            @else
               <div class="col-md-12 text-center">
                  <small class="text-muted">Tidak ada data</small>
               </div>
            @endif
         </div>
      </div>
   </div>
</div>
@endsection