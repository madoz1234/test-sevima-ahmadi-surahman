@extends('layouts.app')

@section('title', $user['username'] . " Post")

@push('css')
   <style>
      .font-size-13 {
         font-size: 13px;
      }
   </style>
@endpush

@section('content')
<div class="container" style="padding-top: 0;padding-left: 0;padding-right: 0;padding-bottom: 0;">
   <div class="col-md-12">
      <img src="{{ asset('storage/' . $post['image']) }}" width="100%" alt="">
      <div class="mt-2 text-right">
         @if ($post['user_id'] == \Auth::user()->id)
            <a href="{{ route('user.post.edit', ['username' => $user['username'], 'id' => $post['id']]) }}" class="font-size-13 text-muted m-2 "><i class="fas fa-pencil-alt"></i> Ubah</a>
            <a href="{{ route('user.post.delete', ['username' => $user['username'], 'id' => $post['id']]) }}" class="font-size-13 text-muted m-2"><i class="fas fa-trash-alt"></i> Hapus</a>
         @endif
      </div>
   </div>
   <div class="col-md-12" style="margin-top: 10px;">
      <img class="img-circle" src="{{ getImage($user['avatar']) }}" width="40px" height="40px" alt="User Avatar">
      <a href="{{ route('user.profile', $user['username']) }}" style="color: black">
         <span class="font-size-13 ml-2">
            {{ $user['username'] }}
         </span>
      </a>
      <div style="text-align: justify;" class="content-body {{ $post->comments->count() > 4 ? ' komentar-scroll' : ' komentar-normal' }}" >
         <span>{{ $post['content'] }}</span>
         @if (!$comments->isEmpty())
            <div style="margin-top: 10px;">
               @foreach ($comments as $comment)
                  <div class="card-footer card-comments" style="background-color: transparent;padding:0;">
                     <div class="card-comment" style="border-bottom-color: transparent;">
                        <img class="img-circle img-sm" src="{{ getImage($comment->users->avatar) }}" alt="User Image">
                        <div class="comment-text">
                           <span class="username">
                              <a href="{{ route('user.profile', $comment->users->username) }}" style="color: black">
                                 {{ $comment->users->name }}
                              </a>
                              <span class="text-muted float-right">{{ date('d/m/Y', strtotime($comment->created_at)) }}
                                 @if ($comment->user_id == \Auth::user()->id)
                                    <div class="delete-comment text-muted text-right">
                                       <i class="fas fa-trash-alt"></i>
                                       <a href="{{ route('delete-comment', $comment->id) }}">Hapus</a>
                                    </div>
                                 @endif
                              </span>
                           </span>
                           <p>
                              {{ $comment->comment }}
                           </p>
                        </div>
                     </div>
                  </div>
               @endforeach
            </div>
         @endif
      </div>
      <div class="card-footer pl-1 pr-1 pb-1 pt-0" style="background-color: transparent;">
         <!--cek keadaan sudah di like atau belum-->
         <button id="like" class="btn border-0 btn-sm mt-1 {{ \Auth::user()->hasLiked($post) ? ' like-post' : '' }}">
            @if(\Auth::user()->hasLiked($post))
               <span style="font-size: 25px;color:red;" class="fas fa-heart"></span>
            @else
               <span style="font-size: 25px;color:black;" class="fas fa-heart"></span>
            @endif
         </button>
         <small class="float-right text-muted" style="margin: 8px 4px 0px 0px" >
            <span id="like-count">{{ $post->likers()->count() }}</span> suka
         </small>
         <form action="{{ route('add-comment') }}" method="post">
            @csrf
            <div class="row">
               <div class="column text-center" style="width: 91%;background-color: transparent;border:1px solid grey;border-radius: 10px;">
                  <input type="text" style="border-radius: 20px;" placeholder="Tambahkan komentar..." class="input-comment" name="comment" id="" required>
                  <input type="hidden" name="post_id" id="post_id" value="{{ $post['id'] }}">
               </div>
               <div class="column text-right" style="width: 9%;background-color: transparent;">
                  <a class="btn-kirim-komentar" onclick="$(this).closest('form').submit()" href="#"><span class="fas fa-paper-plane" style="font-size: 25px;"></span></a>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>
@endsection

@push('js')
   <script>
      $(document).ready(function() {
         $.ajaxSetup({
            headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
         });

         $('#like').click(function() {
            var post_id = $('#post_id').val();
            var like_count = $('#like-count').text();
            var cObj = $(this);
            var baseurl = window.location.origin+window.location.pathname;
            console.log(baseurl)
            $.ajax({
               type:'POST',
               url:'{{ url('add-like') }}',
               data:{id:post_id},
               success:function(data) {
                  if($(cObj).hasClass("like-post")){
                     $('#like-count').text(parseInt(like_count) - 1);
                     $(cObj).removeClass("like-post");
                     $(".fa-heart").css("color", "black");

                  } else {
                     $('#like-count').text(parseInt(like_count) + 1);
                     $(cObj).addClass("like-post");
                     $(".fa-heart").css("color", "red");
                  }
               }
            });
         });
      });
   </script>
@endpush