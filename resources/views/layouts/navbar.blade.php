<div class="container-fluid p-0" style="background-color: transparent;">
   <nav class="navbar navbar-expand" style="background-color: transparent;">
      <div class="container" style="background-color: transparent;">
         <a class="navbar-brand" style="padding-left: 10px;" href="{{ route('dashboard') }}">InstaMed</a>
         <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown" >
               <a class="nav-link" data-toggle="dropdown" href="#" >
                  <img class="img-circle" src="{{ getImage(\Auth::user()->avatar) }}" width="50px" height="50px" alt="User Avatar">                  
                  {{-- <img class="img-circle" src="https://www.shareicon.net/data/512x512/2017/01/06/868320_people_512x512.png" width="30px" height="30px" alt="User Avatar"> --}}
               </a>
               <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                  <div class="card card-widget widget-user" style="margin-bottom: 0px">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header bg-info">
                     <h5>{{ \Auth::user()->username }}</h5>
                  </div>
                  <div class="widget-user-image">
                     <img class="img-circle elevation-2" src="{{ getImage(\Auth::user()->avatar) }}" style="height: 100px; width: 100px" alt="User Avatar">
                     {{-- <img class="img-circle elevation-2" src="https://www.shareicon.net/data/512x512/2017/01/06/868320_people_512x512.png" style="height: 100px; width: 100px" alt="User Avatar"> --}}
                  </div>
                  <div class="card-footer">
                     <div class="row">
                        <div class="col-md-12 mt-2">
                           
                           <a href="{{ route('user.profile',['username' => \Auth::user()->username, 'profile' => 'profile'])}}" class="btn btn-sm btn-default btn-block border-0 text-left">Profile</a>
                           <a href="{{ route('pengaturan') }}" class="btn btn-sm btn-default btn-block border-0 text-left">Pengaturan</a>
                           <hr>
                           <form action="{{ route('logout') }}" method="post">
                              @csrf
                              <input type="submit" class="btn btn-sm btn-default btn-block border-0 text-left" value="Logout">
                           </form>
                        </div>
                        
                     </div>
                        <!-- /.col -->
                        
                  </div>
                     <!-- /.row -->
                  </div>
               </div>
            </li>
         </ul>
         {{-- <div class="nav navbar-nav">
            <a class="nav-item nav-link active" href="#">Home <span class="sr-only">(current)</span></a>
            <a class="nav-item nav-link" href="#">Home</a>
         </div> --}}
      </div>
   </nav>
</div>