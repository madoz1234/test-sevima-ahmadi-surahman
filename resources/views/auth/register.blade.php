@extends('layouts.auth.app')

@section('title', 'Daftar | InstaMed')

@section('content')
<div class="login-page">
    <div class="card" style="width: 100%;height: 100%;">
        <div class="card-body login-card-body text-center justify-content-center" style="background-image: linear-gradient(#74ebd5, #ACB6E5);">
            <div class="card-body login-card-body text-center justify-content-center" style="background-color: transparent;margin-top: 30%;">
                <div class="login-logo">
                    <strong><a href="#">InstaMed</a></strong>
                </div>
                <form action="{{ route('register') }}" method="POST">
                    @csrf
                    <div class="input-group mb-3">
                        <div class="input-group-append">
                            <div class="input-group-text" style="border: none;">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>
                        <input type="text" style="border-radius: 20px;" name="name" class="form-control" placeholder="Masukan nama" autofocus="on" autocomplete="off" required>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-append">
                            <div class="input-group-text" style="border: none;">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>
                        <input type="text" style="border-radius: 20px;" name="username" class="form-control" placeholder="Masukan username" autofocus="on" autocomplete="off" required>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-append">
                            <div class="input-group-text" style="border: none;">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                        <input type="email" style="border-radius: 20px;" name="email" class="form-control" placeholder="Masukan email" autofocus="on" autocomplete="off" required>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-append">
                            <div class="input-group-text" style="border: none;">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                        <input type="password" style="border-radius: 20px;" name="password" class="form-control" placeholder="Masukan password" required>
                    </div>
                    <div class="row mb-3 text-center justify-content-center">
                        <div class="col-6">
                            <button type="submit" style="border-radius: 20px;" class="btn btn-primary btn-block"><strong>Daftar</strong></button>
                        </div>
                    </div>
                </form>
                <div class="login mb-0"><strong>
                    Sudah punya akun ? <a href="{{ route('login') }}" title="Klik untuk ke halaman login">Masuk</a></strong>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection