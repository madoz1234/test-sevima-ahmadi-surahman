@extends('layouts.auth.app')

@section('title', 'Masuk | InstaMed')

@section('content')
<div class="login-page">
    <div class="card" style="width: 100%;height: 100%;">
        <div class="card-body login-card-body text-center justify-content-center" style="background-image: linear-gradient(#74ebd5, #ACB6E5);">
            <div class="card-body login-card-body text-center justify-content-center" style="background-color: transparent;margin-top: 30%;">
                <div class="login-logo"><strong>
                    <a href="#">InstaMed</a></strong>
                </div>
                <p class="login-box-msg">
                    @if (session()->has('login.failed'))
                        <div class="alert alert-danger alert-dismissible text-left">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                {{ session('login.failed') }}
                        </div>
                    @endif
                </p>
                <form action="{{ route('login') }}" method="POST">
                    @csrf
                    <div class="input-group mb-3">
                        <div class="input-group-append">
                            <div class="input-group-text" style="border: none;">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>
                        <input type="text" style="border-radius: 20px;" name="username" class="form-control" placeholder="Masukan username" autofocus="on" autocomplete="off" required>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-append">
                            <div class="input-group-text" style="border: none;">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                        <input type="password" style="border-radius: 20px;" name="password" class="form-control" placeholder="Masukan password" required>
                    </div>
                    <div class="row mb-3 text-center justify-content-center">
                        <div class="col-6">
                            <button type="submit" style="border-radius: 20px;" class="btn btn-primary btn-block"><strong>Masuk</strong></button>
                        </div>
                    </div>
                </form>
                <div class="login mb-0"><strong>
                    Belum punya akun ? Klik <a href="{{ route('register') }}" title="Klik untuk registrasi">Disini</a></strong>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection