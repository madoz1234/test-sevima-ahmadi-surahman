#Setup

1. Clone Repo
2. composer install
3. copy env.example dan rubah menjadi .env, setelah itu konfigurasi database
4. php artisan key:generate
5. php artisan migrate
6. php artisan db:seed
7. php artisan storage:link
8. akses localhost/namaaplikasi/
9. Username Password = admin/password
